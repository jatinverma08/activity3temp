//
//  activity.ino.metal
//  Activity3
//
//  Created by jatin verma on 2019-11-06.
//  Copyright © 2019 jatin verma. All rights reserved.
//

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>



#include "math.h"


InternetButton b = InternetButton();




void setup() {

    // 1. Setup the Internet Button
    b.begin();

    // 2. Setup the API endpoint --
    // When person visits http://particle.io/..../lights, run the controlNumLights() function
    Particle.function("degree", weather);
    Particle.function("time",clock);
    
    // 3. Setup the initial state of the LEDS
    // (by default, turn on 6 lights)
  //  activateLEDS();

}

void loop(){
    
    // This loop just sits here and waits for the numLightsChanged variable to change to true
    // Once it changes to true, run the activateLEDS() function.
  
}




float weather(float cmd){
   
   if(cmd >5&& cmd <10){
   button.allLedsOn(74, 106, 158);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd >1&& cmd <5) {
    button.allLedsOn(132, 173, 240);
    delay(2000);
    button.allLedsOff();
  }
    return 1.0;
}
int clock(int cmd){
   
  int score = cmd.toInt();

  if (score < 0 || score > 11) {
    // error: becaues there are only 11 lights
    // return -1 means an error occurred
    return -1;
  }

  for (int i = 1; i <= score; i++) {
      button.ledOn(i, 255, 255, 0);
  }

  // return = 1 means the function finished running successfully
  return 1;
}

