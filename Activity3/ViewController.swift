//
//  ViewController.swift
//  Activity3
//
//  Created by jatin verma on 2019-11-01.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity
import Particle_SDK

class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    // MARK: User variables
      let USERNAME = "jatin_verma@outlook.com"
      let PASSWORD = "kaur1234"
      
      // MARK: Device
      
      let DEVICE_ID = "3f003b001047363333343437"
      var myPhoton : ParticleDevice?

      // MARK: Other variables
    @IBOutlet weak var resultsLabel: UILabel!
    
    var weather: String = ""
    
    @IBOutlet weak var tomorrowweather: UILabel!
    
    @IBOutlet weak var resultsLabel2: UILabel!
    @IBOutlet weak var name: UILabel!
    
        
    @IBOutlet weak var city: UITextField!
   var location: String = ""
     
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
       
       // 1. When a message is received from the watch, output a message to the UI
       // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
       // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
       DispatchQueue.main.async {
           self.city.insertText("\nMessage Received: \(message["location"])")
       }
       
       // 2. Also, print a debug message to the phone console
       // To make the debug message appear, see Moodle instructions
       print("Received a message from the watch: \(message["location"])")
   }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            // Do any additional setup after loading the view.
        
        if WCSession.isSupported() {
           print("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            print("\nPhone does not support WCSession")
        }
        
        
        
        // Do any additional setup after loading the view.
        // 1. Initialize the SDK
               ParticleCloud.init()
        
               // 2. Login to your account
               ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
                   if (error != nil) {
                       // Something went wrong!
                       print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                       // Print out more detailed information
                       print(error?.localizedDescription)
                   }
                   else {
                       print("Login success!")

                       // try to get the device
                       self.getDeviceFromCloud()

                   }
               } // end login
        }
        
        // Gets the device from the Particle Cloud
        // and sets the global device variable
        func getDeviceFromCloud() {
            ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
                
                if (error != nil) {
                    print("Could not get device")
                    print(error?.localizedDescription)
                    return
                }
                else {
                    print("Got photon from cloud: \(device?.id)")
                    self.myPhoton = device
                    
                    // subscribe to events
                    self.subscribeToParticleEvents()
                }
                
            } // end getDevice()
        }
        
        
        //MARK: Subscribe to "weather" events on Particle
        func subscribeToParticleEvents() {
            var handler : Any?
            handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
                withPrefix: "playerChoice",
                deviceID:self.DEVICE_ID,
                handler: {
                    (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    
                   
                    self.ShowWeather()
                    
                }
            })
        }
    func ShowWeather() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
 
    
       
     
    @IBAction func buttonClicked(_ sender: Any) {
 
        print("Button pressed")
        location = self.city.text!
        
            // 1. Go to sunrise-sunset api
            // & wait for the website to repond
        AF.request("https://api.openweathermap.org/data/2.5/weather?q=\(location)&units=metric&APPID=acebdb4548ced12a28df4ccf585c3466").responseJSON {
            
                (xyz) in
                print(xyz.value)
            
                // convert the response to a JSON object
                let x = JSON(xyz.value)
                let temp = x["main"]["temp"]
                 let tempMax = x["main"]["temp_max"]
              let tempMin = x["main"]["temp_min"]
            let Tomorrow = x["description"]
            let name = x["name"]
                print("TempMax: \(tempMax)")
                print("TempMin: \(tempMin)")
            print("Tomorrow's Prediction: \(Tomorrow)")
            print("Name: \(name)")
                self.resultsLabel.text = "Temp: \(temp)"
            self.tomorrowweather.text = "Tomorrow: \(Tomorrow)"
            self.name.text = "Name: \(name)"
            
        }
            
         //  https://worldtimeapi.org/api/timezone/America/Toronto
        AF.request("https://worldtimeapi.org/api/timezone/America/\(location)").responseJSON {
            
                (xyz1) in
                print(xyz1.value)
            
                // convert the response to a JSON object
                let x2 = JSON(xyz1.value)
               let time = x2["datetime"]
    //             let tempMax = x["main"]["temp_max"]
    //          let tempMin = x["main"]["temp_min"]
    //
    //            print("TempMax: \(tempMax)")
                print("Time: \(time)")
                self.resultsLabel2.text = "time: \(time)"
            }
          
           
        
        }
    
}






