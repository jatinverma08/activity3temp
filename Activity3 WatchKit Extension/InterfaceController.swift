//
//  InterfaceController.swift
//  Activity3 WatchKit Extension
//
//  Created by jatin verma on 2019-11-01.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity
import SwiftyJSON
import Alamofire

class InterfaceController: WKInterfaceController ,WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
   
    @IBAction func torontoButton() {
    if WCSession.default.isReachable {
               print("Attempting to send message to phone")
               print("Sending msg to watch")
               WCSession.default.sendMessage(
                   ["location" : "Toronto"],
                   replyHandler: {
                       (_ replyMessage: [String: Any]) in
                       // @TODO: Put some stuff in here to handle any responses from the PHONE
                       print("Message sent, put something here if u are expecting a reply from the phone")
                       print("Got reply from phone")
               }, errorHandler: { (error) in
                   //@TODO: What do if you get an error
                   print("Error while sending message: \(error)")
                   print("Error sending message")
               })
           }
           else {
               print("Phone is not reachable")
               print("Cannot reach phone")
           }
       }
    
    
    @IBAction func vancouverButton() {
        if WCSession.default.isReachable {
                print("Attempting to send message to phone")
                print("Sending msg to watch")
                WCSession.default.sendMessage(
                    ["location" : "Vancouver"],
                    replyHandler: {
                        (_ replyMessage: [String: Any]) in
                        // @TODO: Put some stuff in here to handle any responses from the PHONE
                        print("Message sent, put something here if u are expecting a reply from the phone")
                        print("Got reply from phone")
                }, errorHandler: { (error) in
                    //@TODO: What do if you get an error
                    print("Error while sending message: \(error)")
                    print("Error sending message")
                })
            }
            else {
                print("Phone is not reachable")
                print("Cannot reach phone")
            }
        }
    
    
   //
    @IBAction func WinnipegButton() {
   
    if WCSession.default.isReachable {
                   print("Attempting to send message to phone")
                   print("Sending msg to watch")
                   WCSession.default.sendMessage(
                       ["location" : "Winnipeg"],
                       replyHandler: {
                           (_ replyMessage: [String: Any]) in
                           // @TODO: Put some stuff in here to handle any responses from the PHONE
                           print("Message sent, put something here if u are expecting a reply from the phone")
                           print("Got reply from phone")
                   }, errorHandler: { (error) in
                       //@TODO: What do if you get an error
                       print("Error while sending message: \(error)")
                       print("Error sending message")
                   })
               }
               else {
                   print("Phone is not reachable")
                   print("Cannot reach phone")
               }
    }
 
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        // 1. Check if teh watch supports sessions
               if WCSession.isSupported() {
                   WCSession.default.delegate = self
                   WCSession.default.activate()
               }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
